"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
" (_)_/ |_|_| |_| |_|_|  \___|
"

" Bootstrap {{{

" Drop vi compatibility
set nocompatible

" Use plugin and syntax highlight
filetype plugin indent on
syntax on

" Setting up Vundle - the vim plugin bundler
let iCanHazVundle=1
let vundle_readme=expand('~/.vim/bundle/vundle/README.md')
if !filereadable(vundle_readme)
    if ! executable("git")
        throw "Unable to find 'git' binary, can't bootstrap Vundle"
    endif
    echo "Installing Vundle..."
    echo ""
    call mkdir(expand('~/.vim/bundle'), 'p')
    silent !git clone https://github.com/gmarik/vundle ~/.vim/bundle/vundle
    call mkdir(expand('~/.vim/bundle/vundle/plugin'))
    let iCanHazVundle=0
endif

" Use Vundle
filetype off
set rtp+=~/.vim/bundle/vundle
call vundle#rc()

" Create undo/backup directory if doesn't exist
let myundodir=expand('~/.vim/undo-files')
if !isdirectory(myundodir)
    call mkdir(myundodir)
endif

" }}}

" Bundle {{{

"" Let vundle manage vundle
Bundle 'gmarik/vundle'

" Solarized colorscheme
Bundle "altercation/vim-colors-solarized"

" Secure modelines
Bundle "ciaranm/securemodelines"

" Commands to switch between source files and header files
Bundle "vim-scripts/a.vim"

" Help for easier tag closing with html/xml
Bundle "vim-scripts/closetag.vim"

" Allow to browse .tar
Bundle "vim-scripts/tar.vim"

" Allow to browse .deb like .tar
Bundle "vim-scripts/deb.vim"

" Setup undofile only for certain files
Bundle "vim-scripts/undofile.vim"

" Allow to open a file in a given line with the syntax filename:line
Bundle "kopischke/vim-fetch"

" Edit large files quickly
Bundle "vim-scripts/LargeFile"

if filereadable('/usr/bin/latex')
    " LaTex IDE in vim
    Bundle "vim-scripts/LaTeX-Suite-aka-Vim-LaTeX"
endif

" Extended % matching
Bundle "vim-scripts/matchit.zip"

" Plugin for intensely orgasmic commenting
Bundle "scrooloose/nerdcommenter"

" A tree explorer
Bundle "scrooloose/nerdtree"

if filereadable('~/.languagetool/languagetool-commandline.jar')
    " Grammar checker (require LanguageTool)
    Bundle "vim-scripts/LanguageTool"
endif

if executable("jad")
    " Allow to open .class using jad (Java Decompiler)
    Bundle "vim-scripts/JavaDecompiler.vim"
endif

if executable("ctags")
    " Use tags to navigate in code (require ctags)
    Bundle "vim-scripts/taglist.vim"

    " C/C++ omni-completion with ctags database
    Bundle "vim-scripts/OmniCppComplete"
endif

" Run interactive programs inside a vim buffer
Bundle "rosenfeld/conque-term"

" Search and display tags in files (like TODO or FIXME)
Bundle "fisadev/FixedTaskList.vim"

" Use tab for insert completion
Bundle "ervandew/supertab"

" Text alignment
Bundle "godlygeek/tabular"

" Helper for Unix commands
Bundle "tpope/vim-eunuch"

" Allow to increase/decrease date with C^A/C^X
Bundle "tpope/vim-speeddating"

" Allow easy surrounding
Bundle "tpope/vim-surround"

" Complementary pairs of mappings
Bundle "tpope/vim-unimpaired"

" Repeat.vim remaps . in a way that plugins can tap into it
Bundle "tpope/vim-repeat"

" Git wrapper
Bundle "tpope/vim-fugitive"

" A gitk clone for vim (require vim-fugitive)
Bundle "gregsexton/gitv"

" Transparent editing of gpg files
Bundle "jamessan/vim-gnupg"

" Better statusline
Bundle "Lokaltog/vim-powerline"

" Edit Files using Sudo/su
Bundle "chrisbra/SudoEdit.vim"

" Adds command and keymappings to interact with git grep from vim
Bundle "aghareza/vim-gitgrep"

" TextObject {{{

" Create your own text objects (required some other text objects plugins)
Bundle "kana/vim-textobj-user"

" Text objects like motion for arguments
Bundle "vim-scripts/argtextobj.vim"

" Text objects for foldings
Bundle "kana/vim-textobj-fold"

" Text objects for date and time
Bundle "kana/vim-textobj-datetime"

" Text objects for manipulating blocks based on their indentation
Bundle "austintaylor/vim-indentobject"

" Text objects for underscore
Bundle "lucapette/vim-textobj-underscore"

" }}}

" Python {{{

" Require vim with python support
if has("python")
    " Plugin to visualize your Vim undo tree
    Bundle "sjl/gundo.vim"

    " Python IDE in vim
    Bundle "davidhalter/jedi-vim"

endif

" Better python indent
Bundle "vim-scripts/indentpython.vim--nianyang"

" Extended % matching for python
Bundle "vim-scripts/python_match.vim"

" Check python source files with flake8
Bundle "nvie/vim-flake8"

" Integrate python documentation in vim
Bundle "fs111/pydoc.vim"

" Sort python imports using isort
Bundle 'fisadev/vim-isort'


" }}}

" Syntax {{{

" iptables file syntax
Bundle "vim-scripts/iptables"

" jinja2 syntax
Bundle "vim-scripts/Jinja"

" JSON syntax
Bundle "vim-scripts/JSON.vim"

" lighttpd syntax
Bundle "vim-scripts/lighttpd-syntax"

" Markdown syntax
Bundle "vim-scripts/Markdown"

" nginx syntax
Bundle "vim-scripts/nginx.vim"

" Brainfuck syntax
Bundle "vim-scripts/brainfuck-syntax"

" i3 syntax
Bundle "PotatoesMaster/i3-vim-syntax"

" Conky syntax
Bundle "smancill/conky-syntax.vim"

" Systemd syntax
Bundle "Matt-Stevens/vim-systemd-syntax"

" Udev syntax
Bundle "vim-scripts/syntaxudev.vim"

" Git syntax (commit message, rebase, ...)
Bundle "tpope/vim-git"

" Salt SLS syntax
Bundle "saltstack/salt-vim"


" }}}

" }}}

" Color {{{

if &term=~'linux'
    set background=dark
    colorscheme desert
else
    " Force 256 colors support
    set t_Co=256
    " Use solarized color theme
    set background=dark
    " /!\ background and syntax on must be defined *BEFORE*
    let g:solarized_termcolors=256
    silent! colorscheme solarized
endif

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=240 guibg=#585858
match ExtraWhitespace /\s\+$/
" Color column
set colorcolumn=100
hi ColorColumn ctermbg=235 guibg=#262626

" }}}

" Linenumber/position options {{{

" Highlight current line
set cursorline
" Show line number
set number
" Show current cursor position
set ruler

" }}}

" Split options {{{

" More natural split opening
set splitbelow
set splitright

" }}}

" Indent/space options {{{
" Number of spaces that a <Tab> in the file counts for
set tabstop=4
" Number of spaces that a <Tab> counts for while performing editing operation
set softtabstop=4
" Number of spaces to use for each step of (auto)indent
set shiftwidth=4
" Use space instead of <Tab>
set expandtab
" Set indent style to BSD KNF
set cinoptions=p0,t0,+4,(0,u4,U1,:0
" Influences the working of <BS>, <Del>, CTRL-W and CTRL-U in Insert mode. This
" is a list of items, separated by commas. Each item allows a way to backspace
" over something.
set backspace=indent,eol,start
" Copy indent from current line when starting a new line (typing <CR> in Insert
" mode or when using the "o" or "O" command).
set autoindent
" Do smart autoindenting when starting a new line. Works for C-like programs,
" but can also be used for other languages.
set smartindent


" This is a sequence of letters which describes how automatic formatting is to
" be done (cf :help 'fo-table').
"
" letter    meaning when present in 'formatoptions'
" ------    ---------------------------------------
" c         Auto-wrap comments using textwidth, inserting the current comment
"           leader automatically.
" q         Allow formatting of comments with "gq".
" r         Automatically insert the current comment leader after hitting
"           <Enter> in Insert mode.
" n         When formatting text, recognize numbered lists.  This actually uses
"           the 'formatlistpat' option, thus any kind of list can be used.
set formatoptions=cqrn

" This is a sequence of letters which describes how to avoid all the hit-enter
" prompts caused by file messages (cf :help 'shortmess').
"
" letter    meaning when present in 'formatoptions'
" ------    ---------------------------------------
" f         Use "(3 of 5)" instead of "(file 3 of 5)"
" x         Use "[dos]" instead of "[dos format]", "[unix]" instead of
"		    [unix format]" and "[mac]" instead of "[mac format]".
" t         Truncate file message at the start if it is too long to fit
"   		on the command-line, "<" will appear in the left most column.
" T         Truncate other messages in the middle if they are too long to
"   		fit on the command line.  "..." will appear in the middle.
" O         Message for reading a file overwrites any previous message.
" o         Overwrite message for writing a file with subsequent message
"   		for reading a file
set shortmess=fxtTOo

" }}}

" Search related options {{{

" When there is a previous search pattern, highlight all its matches.
set hlsearch
" While typing a search command, show immediately where the so far typed
" pattern matches.
set incsearch
" Ignore case in search patterns.
set ignorecase
" Override the 'ignorecase' option if the search pattern contains upper case
" characters.
set smartcase
" Searches wrap around the end of the file.
set wrapscan

" }}}

" Completion options {{{

" Cmd: Complete till longest common string and start wildmenu.
set wildmode=longest:full
set wildmenu
" Text: Complete the longuest common test and display a menu
set completeopt=longest,menuone

" }}}

" General configuration {{{

" Default encoding: utf-8
set encoding=utf-8
set fileencoding=utf8
" Auto switch current file dir
set autochdir
" Show matching bracets when text indicator is over them
set showmatch
set ffs=unix,dos,mac
" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.jpg,.png,.jpeg,.gif,.bak,~,.swp,.swo,.o,.info,.aux,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.pyc,.pyo
" Program to use for the :grep command.
set grepprg=grep\ -nH\ $*
" Spellcheck language
set spelllang=en_us
" Set window title
set title
" Use '*' register for sharing clipboard
set clipboard=unnamed
" Autoreload files
set autoread
" Use visualbell instead of bell
set visualbell
" Persistent undo files for $HOME config files and $HOME/scm dir
au BufReadPre ~/.*,~/scm/**/* setlocal undofile
" Store undo files in one place
set undodir=~/.vim/undo-files,~/tmp,/tmp,/var/tmp
" Do not interpret modeline (use securemodelines)
set nomodeline
" Set default comment type (used by NERD_commenter)
set comments=:# commentstring=#\ %s
" incr/decr letters C-a/C-x
set nrformats+=alpha
" Add < and > to matching pairs
set matchpairs+=<:>
" Don't redraw while executing macros
set lazyredraw
if ( !has("gui_running") )
    set mouse=vhr
endif

" }}}

" Plugins configuration {{{
"
" NERDCommenter
"
" Add a space after the left delimiter and before the right delimiter
let NERDSpaceDelims=1
" Comment the whole line if has no multipart delimiters
let NERDCommentWholeLinesInVMode=2

"
" NERDTree
"
" Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" Add extra syntax highlighting elements
let NERDChristmasTree=1
" Ignore somes files
let NERDTreeIgnore=['\.img$', '\~$', '\.swp$']

"
" Flake8
"
let g:flake8_quickfix_height=30

"
" Supertab
"
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabNoCompleteAfter = ['^', ',', '\s']

"
" Latexsuite
"
" Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor = "latex"
" The default format for viewing files
let g:Tex_DefaultTargetFormat = 'pdf'
" Latex-Suite supports compiling into dvi, ps and pdf by default. All these
" rules are strings defined by default as follows
let g:Tex_CompileRule_dvi = 'latex -interaction=nonstopmode $*'
let g:Tex_CompileRule_ps = 'ps2pdf $*'
let g:Tex_CompileRule_pdf = 'latexmk -pdf -g $*'
let g:Tex_ViewRule_pdf = 'evince'
" Disable smart quotes
let g:Tex_SmartKeyQuote = 0
" This string represents a comma separated list of fields corresponding to
" environments. Pressing <F5> in insert-mode in the body of the document asks
" you to choose from one of these environments to insert.
let g:Tex_PromptedEnvironments='lstlisting,spverbatim,verbatim,center,description,itemize,enumerate,figure'
" The directory to scan for images
let g:Tex_ImageDir='img'
" Add folded environment
let Tex_FoldedEnvironments=',lstlisting,spverbatim'
" Enable spell check and also enable correct syntax matching inside verbatim
" like block (fix latexsuite error)
let g:tex_verbspell = 1

"
" LanguageTool
"
" Disable some annoying rules
let g:languagetool_disable_rules='HUNSPELL_NO_SUGGEST_RULE,FRENCH_WHITESPACE'
let g:languagetool_jar='~/.languagetool/languagetool-commandline.jar'

"
" PowerLine
"
let g:Powerline_symbols='fancy'
" Config with no special font, reseted with :PowerlineClearCache
"let g:Powerline_symbols = 'compatible'
let g:Powerline_colorscheme = 'solarized256'
set laststatus=2

"
" FixedTaskList
"
let g:tlTokenList = ['FIXME', 'TODO', 'XXX', 'DEBUG']

"
" python-mode
"
" Don't run lint on save
let g:pymode_lint_write = 0
" Disable run code plugin (conflict with jedi-vim)
let g:pymode_run = 0
" Disable python folding
let g:pymode_folding = 0

"
" python-jedi
"
let g:jedi#popup_on_dot = 0
" display signature in vim command line to save history integrity
let g:jedi#show_call_signatures = 2
let g:jedi#use_splits_not_buffers = "right"
" Don't try to complete 'from module' with 'import' keyword
let g:jedi#smart_auto_mappings = 0

"
" securemodelines
"
" Display which modeline are ignored
let g:secure_modelines_verbose = 1
" modeline is already disabled
let g:secure_modelines_leave_modeline = 1

"
" vim-fugitive
"
" Set Gdiff split as vertical
set diffopt+=vertical

"
" vim-isort
"
let g:vim_isort_python_version = 'python2'

" }}}

" Functions {{{

" Map key to toggle opt
function! MapToggle(key, opt)
  let cmd = ':set '.a:opt.'! \| set '.a:opt."?\<CR>"
  exec 'nnoremap '.a:key.' '.cmd
  exec 'inoremap '.a:key." \<C-O>".cmd
endfunction
command! -nargs=+ MapToggle call MapToggle(<f-args>)

" }}}

" Keys mapping {{{

" <leader> key
let mapleader = ","
"
" Disable annoying ex mode (Q)
map Q <nop>
" We don't need any help!
inoremap <F1> <nop>
nnoremap <F1> <nop>
vnoremap <F1> <nop>

map <F1> :NERDTreeToggle<CR>
set pastetoggle=<F2>
MapToggle <F3> number
MapToggle <F4> spell
nnoremap <F5> :GundoToggle<CR>
map <F10> :TaskList<CR>

MapToggle <leader>rel relativenumber
" <leader>; temporary disable highlight
nmap <silent> <leader>; :silent noh<CR>
" Display whitespace characters with <leader>s
set listchars=tab:>-,trail:·,eol:$,nbsp:¬
nmap <silent> <leader>s :set nolist!<CR>
" Yank(copy) to system clipboard
noremap <leader>y "+y
" Extra 'clipboard' register
nnoremap <leader>d "_d
vnoremap <leader>d "_d
vnoremap <leader>p "_dP
map <leader>O O<Esc>
map <leader>o o<Esc>
autocmd FileType python map <buffer> <F8> :call Flake8()<CR>

" }}}

" Syntax {{{

aug coding
    au!
    " Options for specific filetype
    au BufEnter *.txt set nocindent nosmartindent noautoindent
    au FileType c,cpp,cc set cindent formatoptions=crqn textwidth=99
    au FileType python set textwidth=99 omnifunc=pythoncomplete#Complete
    au BufEnter *.jinja set filetype=html syntax=jinja
    au BufEnter *i3/config set filetype=i3
aug END

" LaTeX {{{
aug latex
    au FileType tex set textwidth=80 tabstop=2 sw=2 formatoptions=tcrqn
    " Use the same syntactic coloration for \verb and \lstinline
    au FileType tex syn region texZone start="\\lstinline\*\=\z([^\ta-zA-Z]\)" end="\z1\|%stopzone\>"
    au FileType tex syn region texZone start="\\lstinline\*\={" end="}\|%stopzone\>"
    au FileType tex syn region texZone start="\\lstinputlisting\*\={" end="}\|%stopzone\>"
    au FileType tex syn region texZone start="\\lstinputlisting\*\={" end="}\|%stopzone\>"
    au FileType tex syn region texZone   start="\\begin{spverbatim}" end="\\end{spverbatim}\|%stopzone\>"
    " fake mapping to avoid (( remplacment
    au FileType tex call IMAP ('((', '((', 'tex')
    " TIP: if you write your \label's as \label{fig:something}, then if you
    " type in \ref{fig: and press <C-n> you will automatically cycle through
    " all the figure labels. Very useful!
    au FileType tex set iskeyword+=:
    " allow insert European characters (change some mapping)
    au FileType tex imap <C-b> <Plug>Tex_MathBF
    au FileType tex imap <C-c> <Plug>Tex_MathCal
    au FileType tex imap <C-l> <Plug>Tex_LeftRight
    " allow insert 'é' in tex files (mapping error)
    au FileType tex imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
    " Hack to disable buggy mapping that prevent insert European characters
    let b:did_brackets = 1
aug END
" }}}

" }}}

" vim: set sw=4 ts=4 sts=4 et tw=78 foldmethod=marker spell:
