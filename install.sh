#!/bin/sh

# Ensure we are in the script directory to copy vim config
cd $(dirname $0)

cp vimrc ~/.vimrc
vim +BundleInstall
